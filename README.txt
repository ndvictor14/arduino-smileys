This arduino code uses the NeoPixel library to animate a smiley face on a 12/14 pixel circular LED ring. 
The first and third rings are set up to be a green color and the second and fourth ring an orange color. 
The first and third rings animate smiley faces while the second and fourth animate frowns.
(NOTE: For this project two additional LEDs were connected to the LED rings making them 14 pixel rings.)

