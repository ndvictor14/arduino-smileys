#include <Adafruit_NeoPixel.h>

#define HAPPYPIN1 5
#define HAPPYPIN2 6
#define FROWNPIN1 9
#define FROWNPIN2 10
#define EYE1 0
#define EYE2 2
#define FROWNEYE1 12
#define FROWNEYE2 13


// Parameter 1 = number of pixels in strip
// Parameter 2 = Arduino pin number (most are valid)
// Parameter 3 = pixel type flags, add together as needed:
//   NEO_KHZ800  800 KHz bitstream (most NeoPixel products w/WS2812 LEDs)
//   NEO_KHZ400  400 KHz (classic 'v1' (not v2) FLORA pixels, WS2811 drivers)
//   NEO_GRB     Pixels are wired for GRB bitstream (most NeoPixel products)
//   NEO_RGB     Pixels are wired for RGB bitstream (v1 FLORA pixels, not v2)
Adafruit_NeoPixel strip1 = Adafruit_NeoPixel(12, HAPPYPIN1, NEO_GRB + NEO_KHZ800);
Adafruit_NeoPixel strip2 = Adafruit_NeoPixel(12, HAPPYPIN2, NEO_GRB + NEO_KHZ800);

// Frown strips
Adafruit_NeoPixel frownStrip1 = Adafruit_NeoPixel(14, FROWNPIN1, NEO_GRB + NEO_KHZ800);
Adafruit_NeoPixel frownStrip2 = Adafruit_NeoPixel(14, FROWNPIN2, NEO_GRB + NEO_KHZ800);
// IMPORTANT: To reduce NeoPixel burnout risk, add 1000 uF capacitor across
// pixel power leads, add 300 - 500 Ohm resistor on first pixel's data input
// and minimize distance between Arduino and first pixel.  Avoid connecting
// on a live circuit...if you must, connect GND first.

void(* resetFunc) (void) = 0;

void setup() {
    strip1.begin();
    strip2.begin();
    frownStrip1.begin();
    frownStrip2.begin();
    strip1.show();
    strip2.show();
    frownStrip1.show();
    frownStrip2.show();
}

void loop() {
   displayBoard();
 }

void displayBoard(){
  smile(strip2,EYE1,EYE2,5,100);
  frown(frownStrip1, FROWNEYE1, FROWNEYE2, 6, 100);
  smile(strip1, EYE1, EYE2, 5, 100);
  frown(frownStrip2, FROWNEYE1, FROWNEYE2, 6, 100);
  delay(60000); // wait a minute

  resetFunc();  
}

void smile(Adafruit_NeoPixel LEDring, uint8_t eye1, uint8_t eye2, uint8_t smileStart, uint8_t delayTime) {
  uint32_t smileColor = LEDring.Color(30,170,0);
  //uint32_t smileColor = LEDring.Color (255,255,255);
  
  LEDring.setPixelColor(eye1, smileColor);
  LEDring.setPixelColor(eye2, smileColor);
  // smile
  for (uint16_t i = (smileStart + 4) ; i >= smileStart ; i--){
      // Smiley eyes
  
    LEDring.setPixelColor(i, smileColor);
    LEDring.show();
    delay(delayTime);
  } 
   
}

void frown(Adafruit_NeoPixel LEDring, uint8_t eye1, uint8_t eye2, uint8_t frownEnd, uint8_t delayTime) {
  LEDring.begin();
  LEDring.show();
  
  uint32_t frownColor = LEDring.Color(100,20,0); //frown Color dimmer
    // Frown eyes
  LEDring.setPixelColor(FROWNEYE1, frownColor);
  LEDring.setPixelColor(FROWNEYE2, frownColor);
  for (uint16_t i = 0 ; i < frownEnd ; i++){
    LEDring.setPixelColor(i, frownColor);
    LEDring.show();
    delay(delayTime);
  } 
}


